package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.ITaskRepository;
import ru.t1.shevyreva.tm.api.ITaskService;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        Task task = taskRepository.findOneById(id);
        return task;
    }

    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        Task task = taskRepository.findOneByIndex(index);
        return task;
    }

    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    public void removeById(final String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.removeById(id);
    }

    public void removeByIndex(final Integer index) {
        if (index == null || index < 0) return;
        taskRepository.removeByIndex(index);
    }

    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = findOneById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
