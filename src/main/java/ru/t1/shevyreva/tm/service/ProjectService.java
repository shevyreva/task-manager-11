package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.IProjectRepository;
import ru.t1.shevyreva.tm.api.IProjectService;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        Project project = projectRepository.findOneById(id);
        return project;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        Project project = projectRepository.findOneByIndex(index);
        return project;
    }

    @Override
    public void remove(Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void removeById(String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.removeById(id);
    }

    @Override
    public void removeByIndex(Integer index) {
        if (index == null || index < 0) return;
        projectRepository.removeByIndex(index);
    }

    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = findOneById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
