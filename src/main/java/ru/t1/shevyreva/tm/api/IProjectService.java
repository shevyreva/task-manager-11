package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String Id, String name, String description);

}
