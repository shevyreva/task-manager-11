package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(final String Id);

    Project findOneByIndex(final Integer index);

    void remove(final Project project);

    void removeById(final String Id);

    void removeByIndex(final Integer index);

}
