package ru.t1.shevyreva.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showById();

    void showByIndex();

    void showUpdateById();

    void showUpdateByIndex();

    void showRemoveById();

    void showRemoveByIndex();

}
