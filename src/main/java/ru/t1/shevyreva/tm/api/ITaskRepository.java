package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(final String Id);

    Task findOneByIndex(final Integer index);

    void remove(final Task task);

    void removeById(final String Id);

    void removeByIndex(final Integer index);

}
