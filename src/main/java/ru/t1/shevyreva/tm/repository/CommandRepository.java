package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.ICommandRepository;
import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;
import ru.t1.shevyreva.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show about program.");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show program version.");

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");

    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    public static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show commands.");

    public static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show arguments.");

    public static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, null, "Create new project.");

    public static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, null, "Clear project.");

    public static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, null, "Show all projects.");

    public static final Command PROJECT_SHOW_BY_ID = new Command(CommandConst.PROJECT_SHOW_BY_ID, null, "Show project by Id.");

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(CommandConst.PROJECT_SHOW_BY_INDEX, null, "Show project by Index.");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(CommandConst.PROJECT_UPDATE_BY_ID, null, "Update project by Id.");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(CommandConst.PROJECT_UPDATE_BY_ID, null, "Update project by Index.");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(CommandConst.PROJECT_REMOVE_BY_ID, null, "Remove project by Id.");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(CommandConst.PROJECT_REMOVE_BY_ID, null, "Remove project by Index.");

    public static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, null, "Create new task.");

    public static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, null, "Clear task.");

    public static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, null, "Show all tasks.");

    public static final Command TASK_SHOW_BY_ID = new Command(CommandConst.TASK_SHOW_BY_ID, null, "Show task by id.");

    public static final Command TASK_SHOW_BY_INDEX = new Command(CommandConst.TASK_SHOW_BY_INDEX, null, "Show task by index.");

    public static final Command TASK_REMOVE_BY_ID = new Command(CommandConst.TASK_REMOVE_BY_ID, null, "Remove task by id.");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(CommandConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index.");

    public static final Command TASK_UPDATE_BY_ID = new Command(CommandConst.TASK_UPDATE_BY_ID, null, "Update task by id.");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(CommandConst.TASK_UPDATE_BY_INDEX, null, "Update task by index.");

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, INFO, VERSION, COMMANDS, ARGUMENTS,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            TASK_CREATE, TASK_LIST, TASK_CLEAR, TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            EXIT};

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
