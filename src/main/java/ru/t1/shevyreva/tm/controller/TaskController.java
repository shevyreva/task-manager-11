package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.ITaskService;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ru.t1.shevyreva.tm.api.ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[Enter name]: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Enter description]: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    public void showRemoveById() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("[ENTER ID]:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
        System.out.println("[OK]");
    }

    public void showRemoveByIndex() {
        System.out.println("[REMOVE BY INDEX]");
        System.out.println("[ENTER INDEX]:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
        System.out.println("[OK]");
    }

    public void showUpdateById() {
        System.out.println("[UPDATE BY ID]");
        System.out.println("[ENTER ID]:");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]:");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    public void showUpdateByIndex() {
        System.out.println("[UPDATE BY INDEX]");
        System.out.println("[ENTER INDEX]:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]:");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    public void showByIndex() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("[ENTER INDEX]:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    public void showById() {
        System.out.println("[SHOW BY ID]");
        System.out.println("[ENTER ID]:");
        final String id = TerminalUtil.nextLine();
        Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("[TASK ID]: " + task.getId());
        System.out.println("[TASK NAME]: " + task.getName());
        System.out.println("[TASK DESCRIPTION]: " + task.getDescription());
    }

}
