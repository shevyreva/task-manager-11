package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.IProjectController;
import ru.t1.shevyreva.tm.api.IProjectService;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[Enter name]: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Enter description]: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    public void showRemoveById() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("[ENTER ID]:");
        final String id = TerminalUtil.nextLine();
        projectService.removeById(id);
        System.out.println("[OK]");
    }

    public void showRemoveByIndex() {
        System.out.println("[REMOVE BY INDEX]");
        System.out.println("[ENTER INDEX]:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.removeByIndex(index);
        System.out.println("[OK]");
    }

    public void showUpdateById() {
        System.out.println("[UPDATE BY ID]");
        System.out.println("[ENTER ID]:");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]:");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    public void showUpdateByIndex() {
        System.out.println("[UPDATE BY INDEX]");
        System.out.println("[ENTER INDEX]:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    public void showByIndex() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("[ENTER INDEX]:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    public void showById() {
        System.out.println("[SHOW BY ID]");
        System.out.println("[ENTER ID]:");
        final String id = TerminalUtil.nextLine();
        Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("[PROJECT ID]: " + project.getId());
        System.out.println("[PROJECT NAME]: " + project.getName());
        System.out.println("[PROJECT DESCRIPTION]: " + project.getDescription());
    }

}
